# Huggg Frontend Coding Challenge
Thanks for taking the time to review my project. Please find below details on how to get it up and running:

### Running the project
```
$ yarn install && yarn start
```
### Running the tests
```
$ yarn test
```
