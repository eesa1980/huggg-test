import styled from 'styled-components'

export const Container = styled.main`
  padding: 20px;
  margin: auto;
  font-size: 15px;
  max-width: 800px;

  * {
    font-family: 'Roboto', sans-serif;
  }

  h1 {
    text-align: center;
    font-size: 20px;
    margin-bottom: 30px;
  }
`
