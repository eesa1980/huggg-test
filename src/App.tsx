import React, { FC, useState } from 'react'
import { FetchToken } from './component/FetchToken'
import { FetchBrands } from './component/FetchBrands'
import { Brands } from './component/Brands'
import { Container } from './App.styled'
import { Brand as BrandType } from './types'
import { BrandDetail } from './component/BrandDetail'

const App: FC = () => {
  const [selected, setSelected] = useState<BrandType | null>(null)

  return (
    <Container>
      <h1>Huggg Coding Test</h1>
      <FetchToken>
        {({ access_token }) => (
          <FetchBrands
            token={access_token}
            ids={[
              '9fd7ed19-1dbf-11ea-b97e-02c6bf374af0',
              'a9ebeb9a-8d0b-41f6-9123-cf12dc1c8fae',
              'a5b5876e-ad65-4260-8c3d-64fe6cb57bb3',
              '700557f9-b39a-415c-ac8b-be66e825ef8d'
            ]}
            embed={['stores']}
          >
            {(brands) => (
              <Brands
                brands={brands}
                selectedId={selected?.id}
                setSelected={setSelected}
              />
            )}
          </FetchBrands>
        )}
      </FetchToken>

      {selected ? <BrandDetail brand={selected} /> : <></>}
    </Container>
  )
}

export default App
