import axios, { AxiosResponse } from 'axios'
import {
  Embedded,
  GetBrandsArgs,
  GetBrandsResponse,
  GetTokenResponse
} from '../types'

export const BASE_URL = 'https://api.huggg.me'

export const Endpoint = {
  TOKEN: '/oauth/token',
  BRAND: '/api/v2/brands'
} as const

export const ApiCalls = () => {
  const getToken = async (): Promise<GetTokenResponse> => {
    const response: AxiosResponse<GetTokenResponse> = await axios({
      baseURL: BASE_URL,
      url: Endpoint.TOKEN,
      method: 'POST',
      data: {
        grant_type: 'client_credentials',
        client_id: 'kf3fBAQ4PmPMfFbe',
        client_secret: 'sDt8gPFJ2PAv5WBBmfcCnzQ4EZtq3KUT'
      }
    })

    return response.data
  }

  const getBrands = async ({
    token,
    ids,
    embed
  }: GetBrandsArgs): Promise<GetBrandsResponse> => {
    let params = '?'
    ids?.forEach((id: string) => (params += `id[]=${id}&`))
    embed?.forEach((e: string) => (params += `embed[]=${e}&`))

    const response: AxiosResponse<GetBrandsResponse> = await axios({
      baseURL: BASE_URL,
      url: Endpoint.BRAND + params.slice(0, -1), // removes last extra char from string
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + token
      },
      params
    })

    // Mutate object to add the embedded items to a particular brand
    response.data.data = response.data.data.map((brand) => {
      brand.embedded = {}

      // Filter embedded items by the ids found in the brand and add the full embedded object to the brand
      embed?.forEach((em) => {
        brand.embedded[em as keyof Embedded] = response.data.embedded[
          em as keyof Embedded
        ]?.filter((item) => {
          return brand[em as keyof Embedded].includes(item.id)
        })
      })

      return brand
    })

    return response.data
  }

  return {
    getToken,
    getBrands
  }
}
