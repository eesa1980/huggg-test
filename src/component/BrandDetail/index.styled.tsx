import styled from 'styled-components'

export const Container = styled.section`
  margin-top: 20px;

  h2 {
    font-size: 20px;
    margin-bottom: 10px;
  }

  p {
    margin-bottom: 10px;
  }
`
