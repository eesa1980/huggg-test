import React from 'react'
import { BrandDetail } from './index'
import { render, screen } from '../../setupTests'
import { dummyBrandsResponse } from '../../util/dummydata'

describe('Map.tsx', () => {
  it('should render BrandDetail component with map', async () => {
    render(<BrandDetail brand={dummyBrandsResponse.data[0]} />)

    screen.getByText('Caffè Nero')
    screen.getByText('Premium award winning Italian coffee.')
    screen.getByText('1 Stores:')
    await screen.findByTitle('Caffè Nero (Frith St)')
    await screen.findByLabelText('location marker')
  })

  it('should render BrandDetail component without map', async () => {
    render(<BrandDetail brand={dummyBrandsResponse.data[1]} />)

    screen.getByText('Barefoot Bakery Online')
    screen.getByText('Freshly baked brownies delivered to your door.')

    await expect(
      screen.findByLabelText('location marker')
    ).rejects.toThrowError()
  })
})
