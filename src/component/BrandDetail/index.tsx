import React from 'react'
import { Map } from '../Map'
import { Brand } from '../../types'
import { Container } from './index.styled'

type PropTypes = {
  brand: Brand
}

export const BrandDetail: React.FC<PropTypes> = ({ brand }) => {
  return (
    <Container>
      <h2>
        <a href={brand.website} target={'__blank'}>
          {brand.name}
        </a>
      </h2>
      <p>{brand.description}</p>
      <p>{brand.default_location_description_markdown}</p>
      <p>
        <a href={`mailto:${brand.website}`} target={'__blank'}>
          {brand.email}
        </a>
      </p>

      {brand.embedded.stores?.length ? (
        <>
          <br />
          <p>{brand.embedded.stores.length} Stores:</p>
          <Map
            locations={brand.embedded.stores.map((s) => ({
              lat: s.latitude,
              lng: s.longitude,
              imageUrl: brand.map_pin_url,
              title: s.name
            }))}
          />
        </>
      ) : (
        <></>
      )}
    </Container>
  )
}
