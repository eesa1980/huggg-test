import styled from 'styled-components'

export const Container = styled.div`
  display: grid;
  grid-auto-rows: auto;
  grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
  gap: 20px 20px;
  justify-items: stretch;
`

export const Brand = styled.button`
  background: none;
  color: inherit;
  font: inherit;
  cursor: pointer;
  outline: inherit;

  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0;
  flex-direction: column;
  border: 3px solid #e0e0e0;
  border-radius: 20px;
  padding: 20px;
  transition: all 0.4s;

  &[aria-selected='true'] {
    border-color: #e5373455;
  }

  :hover {
    border-color: #e53734;
  }

  img {
    width: 100%;
    height: 70px;
    object-fit: contain;
    object-position: center;
    transition: transform 0.4s;
  }

  .caption {
    margin-top: 20px;
    font-weight: bold;
  }
`
