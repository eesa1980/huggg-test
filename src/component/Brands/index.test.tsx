import { render, screen } from '../../setupTests'
import { Brands } from './index'
import React from 'react'
import { dummyBrandsResponse } from '../../util/dummydata'

describe('Brands.tsx', () => {
  it('should render Brands component', async () => {
    const setSelected = jest.fn()

    render(<Brands brands={dummyBrandsResponse} setSelected={setSelected} />)

    const buttons = await screen.findAllByRole('listitem')

    buttons.forEach((btn) => {
      btn.click()
    })

    expect(setSelected).toHaveBeenCalledTimes(buttons.length)
  })
})
