import React from 'react'
import { Brand as BrandType, GetBrandsResponse } from '../../types'
import { Brand, Container } from './index.styled'

type PropTypes = {
  brands: GetBrandsResponse
  setSelected: React.Dispatch<React.SetStateAction<BrandType | null>>
  selectedId?: string
}

export const Brands: React.FC<PropTypes> = ({
  brands,
  setSelected,
  selectedId
}) => {
  return (
    <Container role={'list'}>
      {brands.data.map((brand, index) => (
        <Brand
          aria-selected={selectedId === brand.id}
          key={brand.id}
          onClick={() => setSelected(brands.data[index])}
          role={'listitem'}
          aria-label={`${brand.name}: Click to see more detail`}
        >
          <img src={brand.logo_url} alt={brand.name} title={brand.name} />
          <div role={'caption'} className={'caption'}>
            <h2>{brand.name}</h2>
          </div>
        </Brand>
      ))}
    </Container>
  )
}
