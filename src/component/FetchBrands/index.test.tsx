import mockserver from '../../util/mockserver'
import { render, screen } from '../../setupTests'
import { FetchBrands } from './index'
import React from 'react'
import { rest } from 'msw'
import { BASE_URL, Endpoint } from '../../api'
import { SetupServerApi } from 'msw/node'

const Success = () => <h1>Fetched Brands Success</h1>

describe('FetchToken.tsx', () => {
  let server: SetupServerApi

  it('should render child component on FetchBrands success', async () => {
    server = mockserver()
    server.listen()
    render(<FetchBrands token={'token'}>{() => <Success />}</FetchBrands>)
    await expect(
      screen.findByText('Fetched Brands Success')
    ).resolves.toBeDefined()
    server.close()
  })

  it('should fail to render child component as no valid token exists', async () => {
    server = mockserver(
      rest.get(BASE_URL + Endpoint.BRAND, (req, res, ctx) => {
        return res(ctx.status(500))
      })
    )

    server.listen()
    render(<FetchBrands token={'token'}>{() => <Success />}</FetchBrands>)
    await expect(
      screen.findByText('Fetched Brands Success')
    ).rejects.toThrowError()
    server.close()
  })
})
