import React, { useEffect, useState } from 'react'
import { ApiCalls } from '../../api/'
import { GetBrandsResponse, Token } from '../../types'

type PropTypes = {
  token: Token['access_token']
  ids?: string[]
  embed?: string[]
  children: (props: GetBrandsResponse) => React.ReactElement
}

const apiCalls = ApiCalls()

export const FetchBrands: React.FC<PropTypes> = ({
  ids,
  children,
  token,
  embed
}) => {
  const [brands, set] = useState<GetBrandsResponse | null>(null)

  useEffect(() => {
    apiCalls
      .getBrands({ token, ids, embed })
      .then((brands: GetBrandsResponse) => set(brands))
      .catch(console.error)
  }, [])

  return brands ? children(brands) : <div>Fetching Brands...</div>
}
