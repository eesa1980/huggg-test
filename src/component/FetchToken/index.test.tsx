import mockserver from '../../util/mockserver'
import { render, screen } from '../../setupTests'
import { FetchToken } from './index'
import React from 'react'
import { rest } from 'msw'
import { BASE_URL, Endpoint } from '../../api'
import { SetupServerApi } from 'msw/node'

const Success = () => <h1>Token Success</h1>

describe('FetchToken.tsx', () => {
  let server: SetupServerApi

  it('should render child component on FetchToken success', async () => {
    server = mockserver()
    server.listen()
    render(<FetchToken>{() => <Success />}</FetchToken>)
    await expect(screen.findByText('Token Success')).resolves.toBeDefined()
    server.close()
  })

  it('should fail to render child component as FetchToken call fails with 500 error', async () => {
    server = mockserver(
      rest.post(BASE_URL + Endpoint.TOKEN, (req, res, ctx) => {
        return res(ctx.status(500))
      })
    )

    server.listen()
    render(<FetchToken>{() => <Success />}</FetchToken>)
    await expect(screen.findByText('Token Success')).rejects.toThrowError()
    server.close()
  })
})
