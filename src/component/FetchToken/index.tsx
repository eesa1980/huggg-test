import React, { useEffect, useState } from 'react'
import { ApiCalls } from '../../api/'
import { Token } from '../../types'

type PropTypes = {
  children: (props: Token) => React.ReactElement
}

const apiCalls = ApiCalls()

export const FetchToken: React.FC<PropTypes> = (props) => {
  const [token, set] = useState<Token | null>(null)

  useEffect(() => {
    apiCalls
      .getToken()
      .then((token: Token) => set(token))
      .catch(console.error)
  }, [])

  return token?.access_token ? (
    props.children(token)
  ) : (
    <div>Fetching Token...</div>
  )
}
