import styled from 'styled-components'

export const Container = styled.div`
  height: 300px;
  width: 100%;
  overflow: hidden;
`
