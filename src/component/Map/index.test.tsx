import React from 'react'
import { render, screen } from '../../setupTests'
import { Map } from './index'

describe('Map.tsx', () => {
  it('should render Map component with no images', async () => {
    render(
      <Map
        locations={[
          {
            lat: '0',
            lng: '0',
            title: 'location 1'
          },
          {
            lat: '50',
            lng: '50',
            title: 'location 2'
          }
        ]}
      />
    )

    await screen.findByText('location 1')
    await screen.findByText('location 2')
    await expect(
      screen.findAllByLabelText('location marker')
    ).resolves.toHaveLength(2)
  })

  it('should render Map component with images', async () => {
    render(
      <Map
        locations={[
          {
            lat: '0',
            lng: '0',
            title: 'location 1',
            imageUrl: 'https://via.placeholder.com/150'
          },
          {
            lat: '50',
            lng: '50',
            title: 'location 2',
            imageUrl: 'https://via.placeholder.com/150'
          }
        ]}
      />
    )

    await screen.findByTitle('location 1')
    await screen.findByTitle('location 2')
    await expect(screen.findAllByRole('img')).resolves.toHaveLength(2)
  })
})
