import React from 'react'
import GoogleMapReact from 'google-map-react'
import { Container } from './index.styled'

type Location = {
  lat: string
  lng: string
  imageUrl?: string
  title: string
}

type PropTypes = {
  locations: Location[]
}

const Marker: React.FC<Location> = ({ imageUrl, title }) =>
  imageUrl ? (
    <img
      width={50}
      height={50}
      src={imageUrl}
      title={title}
      alt={title}
      aria-label={'location marker'}
    />
  ) : (
    <div aria-label={'location marker'}>{title}</div>
  )

export const Map: React.FC<PropTypes> = ({ locations }) => {
  return (
    <Container>
      <GoogleMapReact
        bootstrapURLKeys={{ key: '' }}
        defaultCenter={{
          lat: 51.477928,
          lng: -0.001545
        }}
        defaultZoom={10}
      >
        {locations.map((l, index) => (
          <Marker
            key={index}
            lat={l.lat}
            lng={l.lng}
            imageUrl={l.imageUrl}
            title={l.title}
          />
        ))}
      </GoogleMapReact>
    </Container>
  )
}
