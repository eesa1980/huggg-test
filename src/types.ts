export type Token = {
  access_token: string
  expires_in: number
}

export type GetTokenResponse = Token & {
  token_type: 'Bearer'
}

export type GetBrandsArgs = {
  token: string
  ids?: Array<string>
  embed?: Array<string>
}

export type GetBrandsResponse = {
  data: Brand[]
  embedded: Embedded
  current_page: number
  from: number
  last_page: number
  next_page_url?: string | null
  path: string
  per_page: number
  prev_page_url?: string | null
  to: number
  total: number
}

export type Brand = {
  id: string
  created_at: string
  updated_at: string
  name: string
  internal_name: string
  logo: string
  colour: string
  success: string
  share: string
  weight: number
  deleted_at?: string | null
  expiry: number
  website: string
  integration_id: number
  user_id: string
  email?: string | null
  vat: number
  faq?: string | null
  description: string
  redeem?: string | null
  location_text: string
  map_pin_url: string
  consolidated: number
  default_location_description_markdown: string
  logo_url: string
  products: string[]
  consolidated_products: string[]
  stores: string[]
  embedded: Embedded
}

export type Store = {
  id: string
  brand_id: string
  latitiude: string
  longitude: string
  website?: string
  name: string
  description: string
  visible: number
  description_markdown: string
  image: string
  image_url: string
  latitude: string
}

export type Embedded = {
  stores?: Store[]
}
