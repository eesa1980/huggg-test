import { GetBrandsResponse } from '../types'

export const dummyBrandsResponse: GetBrandsResponse = {
  data: [
    {
      id: 'a9ebeb9a-8d0b-41f6-9123-cf12dc1c8fae',
      created_at: '2019-03-27 17:16:36',
      updated_at: '2021-07-30 13:24:54',
      name: 'Caffè Nero',
      internal_name: 'Caffè Nero B2B',
      logo: '2f9fd88cfd5c5d2d9c14a8738a5c3b6dce77356e.png',
      colour: '',
      success:
        'You top human, you. \nBig question is... who gets your next one?',
      share:
        "I'm using @huggg_uk to send my friends & family a little surprise pick me up! Download and send a huggg too @ api.huggg.me/download",
      weight: 1000,
      deleted_at: null,
      expiry: 365,
      website: 'https://caffenero.com/uk/',
      integration_id: 7,
      user_id: '',
      email: null,
      vat: 0,
      faq: null,
      description: '',
      redeem: null,
      location_text: 'the UK',
      map_pin_url:
        'https://cdn.huggg.me/locations/399df50bceaaf43fdf50edcdfd5d3c3494cb7db1.png',
      consolidated: 0,
      default_location_description_markdown:
        'Premium award winning Italian coffee.',
      logo_url:
        'https://cdn.huggg.me/brands/2f9fd88cfd5c5d2d9c14a8738a5c3b6dce77356e.png',
      products: ['2ff2d548-0d28-4a9a-8adb-c9de6a45815c'],
      consolidated_products: [],
      stores: ['13b736bd-b98b-11eb-ac2a-02c96425ec89'],
      embedded: {
        stores: [
          {
            brand_id: 'a9ebeb9a-8d0b-41f6-9123-cf12dc1c8fae',
            description: 'Award winning Italian coffee',
            description_markdown: '',
            id: '13b736bd-b98b-11eb-ac2a-02c96425ec89',
            image: '399df50bceaaf43fdf50edcdfd5d3c3494cb7db1.png',
            image_url:
              'https://cdn.huggg.me/locations/399df50bceaaf43fdf50edcdfd5d3c3494cb7db1.png',
            latitiude: '51.5132621',
            latitude: '51.5132621',
            longitude: '-0.1314045',
            name: 'Caffè Nero (Frith St)',
            visible: 1
          }
        ]
      }
    },
    {
      id: '700557f9-b39a-415c-ac8b-be66e825ef8d',
      created_at: '2020-06-01 16:02:16',
      updated_at: '2021-07-01 18:03:41',
      name: 'Barefoot Bakery Online',
      internal_name: 'Barefoot Bakery Online',
      logo: '7ab638b2eed2882b84f677d8dc0c5258c9a29b17.png',
      colour: '',
      success:
        'You top human, you. \nBig question is... who gets your next one?',
      share:
        "I'm using @huggg_uk to send my friends & family a little surprise pick me up! Download and send a huggg too @ api.huggg.me/download",
      weight: 7,
      deleted_at: null,
      expiry: 365,
      website: 'https://www.barefootoxford.co.uk/',
      integration_id: 7,
      user_id: '',
      email: 'online@barefootoxford.co.uk',
      vat: 0,
      faq: null,
      description: '',
      redeem: null,
      location_text: 'UK',
      map_pin_url: '',
      consolidated: 0,
      default_location_description_markdown:
        'Freshly baked brownies delivered to your door.',
      logo_url:
        'https://cdn.huggg.me/brands/7ab638b2eed2882b84f677d8dc0c5258c9a29b17.png',
      products: [
        '38849368-9fd0-49a1-a6cc-39559f40d1a9',
        '7201b1bb-be28-4ff6-89cf-90d8b98781cd',
        'f976a2cc-d2c5-44b7-8e5e-89506c829c3f'
      ],
      consolidated_products: [],
      stores: [],
      embedded: {}
    }
  ],
  embedded: {},
  current_page: 1,
  from: 1,
  last_page: 1,
  next_page_url: null,
  path: 'https://api.huggg.me/api/v2/brands?0=%3Fid%5B%5D%3D9fd7ed19-1dbf-11ea-b97e-02c6bf374af0%26id%5B%5D%3Da9ebeb9a-8d0b-41f6-9123-cf12dc1c8fae%26id%5B%5D%3Da5b5876e-ad65-4260-8c3d-64fe6cb57bb3%26id%5B%5D%3D700557f9-b39a-415c-ac8b-be66e825ef8d%26embed%5B%5D%3Dstores%26&embed%5B0%5D=stores&id%5B0%5D=9fd7ed19-1dbf-11ea-b97e-02c6bf374af0&id%5B1%5D=a9ebeb9a-8d0b-41f6-9123-cf12dc1c8fae&id%5B2%5D=a5b5876e-ad65-4260-8c3d-64fe6cb57bb3&id%5B3%5D=700557f9-b39a-415c-ac8b-be66e825ef8d',
  per_page: 20,
  prev_page_url: null,
  to: 2,
  total: 2
}
