import { DefaultRequestBody, MockedRequest, RequestHandler, rest } from 'msw'
import { BASE_URL, Endpoint } from '../api/'
import { setupServer } from 'msw/node'

type RequestHandlers = RequestHandler<
  Record<string, any>,
  MockedRequest<DefaultRequestBody>,
  any,
  MockedRequest<DefaultRequestBody>
>

const mockserver = (...requestHandler: RequestHandlers[]) =>
  setupServer(
    ...requestHandler,
    rest.post(BASE_URL + Endpoint.TOKEN, (req, res, ctx) => {
      return res(
        ctx.json({
          access_token: 'access_token',
          expires_in: 3600,
          token_type: 'Bearer'
        })
      )
    }),
    rest.get(BASE_URL + Endpoint.BRAND, (req, res, ctx) => {
      return res(
        ctx.json({
          data: [
            {
              id: 'a9ebeb9a-8d0b-41f6-9123-cf12dc1c8fae',
              created_at: '2019-03-27 17:16:36',
              updated_at: '2021-07-30 13:24:54',
              name: 'Caffè Nero',
              internal_name: 'Caffè Nero B2B',
              logo: '2f9fd88cfd5c5d2d9c14a8738a5c3b6dce77356e.png',
              colour: '',
              success:
                'You top human, you. \nBig question is... who gets your next one?',
              share:
                "I'm using @huggg_uk to send my friends & family a little surprise pick me up! Download and send a huggg too @ api.huggg.me/download",
              weight: 1000,
              deleted_at: null,
              expiry: 365,
              website: 'https://caffenero.com/uk/',
              integration_id: 7,
              user_id: '',
              email: null,
              vat: 0,
              faq: null,
              description: '',
              redeem: null,
              location_text: 'the UK',
              map_pin_url:
                'https://cdn.huggg.me/locations/399df50bceaaf43fdf50edcdfd5d3c3494cb7db1.png',
              consolidated: 0,
              default_location_description_markdown:
                'Premium award winning Italian coffee.',
              logo_url:
                'https://cdn.huggg.me/brands/2f9fd88cfd5c5d2d9c14a8738a5c3b6dce77356e.png',
              products: ['2ff2d548-0d28-4a9a-8adb-c9de6a45815c'],
              consolidated_products: [],
              stores: [
                '13b736bd-b98b-11eb-ac2a-02c96425ec89',
                '13b73a7f-b98b-11eb-ac2a-02c96425ec89',
                '13b73cb2-b98b-11eb-ac2a-02c96425ec89',
                '13b73dc4-b98b-11eb-ac2a-02c96425ec89',
                '13b73ed3-b98b-11eb-ac2a-02c96425ec89',
                '13b73faa-b98b-11eb-ac2a-02c96425ec89'
              ]
            }
          ],
          embedded: [],
          current_page: 1,
          from: 1,
          last_page: 1,
          next_page_url: null,
          path: 'https://api.huggg.me/api/v2/brands?id%5B0%5D=a9ebeb9a-8d0b-41f6-9123-cf12dc1c8fae&0=%3Fid%5B%5D%3Da9ebeb9a-8d0b-41f6-9123-cf12dc1c8fae%26',
          per_page: 20,
          prev_page_url: null,
          to: 1,
          total: 1
        })
      )
    })
  )

export default mockserver
